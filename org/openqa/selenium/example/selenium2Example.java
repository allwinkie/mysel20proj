package org.openqa.selenium.example;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;

public class selenium2Example {
	 public static void main(String[] args) {
	    // Create a new instance of the Firefox driver
	    // Notice that the remainder of the code relies on the interface, 
	    // not the implementation.
	//    WebDriver driver = new FirefoxDriver();
	    WebDriver driver = new ChromeDriver();
	    driver.get("http://www.cnn.com");
	    // And now use this to visit Google
	
	    // Alternatively the same thing can be done like this
	    // driver.navigate().to("http://www.google.com");
	
	    // Find the text input element by its name
	   // WebElement element = driver.findElement(By.id("cnn_ftrcntntinner"));
	    if(driver.findElement(By.xpath("cnn_ftrcntntinner"))!= null){
	    	System.out.println("Element is Present");
	    	}else{
	    	System.out.println("Element is Absent");
	    	}
	    // Enter something to search for
	   // element.sendKeys("Cheese!");
	
	    // Now submit the form. WebDriver will find the form for us from the element
	  //  element.submit();
	
	   // // Check the title of the page
	    System.out.println("Page title is: " + driver.getTitle());
	    
	   // // Google's search is rendered dynamically with JavaScript.
	   // // Wait for the page to load, timeout after 10 seconds
	 //   (new WebDriverWait(driver, 10)).until(new ExpectedCondition<Boolean>() {
	 //       public Boolean apply(WebDriver d) {
	 //           return d.getTitle().toLowerCase().startsWith("cheese!");
	    //    }
	//    });
	
	    // Should see: "cheese! - Google Search"
	    System.out.println("Page title is: " + driver.getTitle());
	    
	    //Close the browser
	    driver.quit();
	}
	}

